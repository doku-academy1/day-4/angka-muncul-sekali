import java.util.*;

public class Main {
    public static void main(String[] args) {
        String angka;

        Scanner keyboard = new Scanner(System.in);
        System.out.print("Masukkan angka: ");
        angka = keyboard.nextLine();
        List<Integer> listNumber = convertInt(angka);

        Map<Integer, Integer> finalList = new HashMap<>();
        for (int i = 0; i<listNumber.size(); i++) {
            if (finalList.containsKey(listNumber.get(i))) {
                finalList.put(listNumber.get(i), finalList.get(listNumber.get(i))+1);
            } else {
                finalList.put(listNumber.get(i), 1);
            }
        }

        List<Integer> answerList = new ArrayList<>();
        for (Integer key: finalList.keySet()) {
            if (finalList.get(key) == 1) {
                answerList.add(key);
            }
        }

        System.out.println(answerList);
    }

    public static List<Integer> convertInt(String number) {
        List<Integer> listNumber = new ArrayList<Integer>();
        for (int i = 0; i<number.length(); i++) {
            char c = number.charAt(i);
            Integer realNumber = Integer.valueOf(Character.toString(c));
            listNumber.add(realNumber);
        }
        return listNumber;
    }
}